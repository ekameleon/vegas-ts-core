import isObject from './isObject'

const isPureObject = ( value:any ):boolean => isObject(value) && Object.prototype.toString.call(value) === '[object Object]' ;

/**
 * Returns `true` if the given object was created by the Object constructor.
 * @name isPlainObject
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given object was created by the Object constructor.
 * @example
 * console.log(isPlainObject({}); // true
 * console.log(isPlainObject([]); // true
 * console.log(isPlainObject(Object.create({})); // true
 * console.log(isPlainObject(Object.create(Object.prototype)); // true
 */
const isPlainObject = ( value:any ):boolean =>
{
    let ctor, proto ;

    if( !isPureObject(value) )
    {
        return false;
    }

    ctor = value.constructor;
    if( typeof ctor !== 'function' )
    {
        return false ;
    }

    proto = ctor.prototype;
    if( (!isPureObject(proto)) || proto.hasOwnProperty('isPrototypeOf') === false )
    {
        return false;
    }

    return true;
};

export default isPlainObject ;
