/**
 * Indicates if the specified character is an <strong>{@link https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange|ASCII}</strong> character.
 * @param {string} c - The character expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return {boolean} <code>true</code> if the specified character is a <strong>{@link https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange|ASCII}</strong> character.
 * @memberof core.chars
 * @name isASCII
 * @function
 * @instance
 * @example
 * console.log( isASCII( "Z" ) ) ; // true
 * console.log( isASCII( "a" ) ) ; // true
 * console.log( isASCII( "+" ) ) ; // true
 */
export function isASCII( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    return c.charCodeAt( 0 ) <= 255 ;
}

export default isASCII;
