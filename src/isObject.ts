/**
 * Indicates if the specific value is an Object.
 * (e.g. objects, arrays, functions, regexes, `new Number(0)`, and `new String('')`)
 * @name isObject
 * @memberof core
 * @function
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 * @param {Object} value - The value to check.
 * @return {boolean} <code>true</code> if the object is an Object.
 * @example
 * console.log( isObject(0) ) ; // false
 * console.log( isObject(true) ) ; // false
 * console.log( isObject(null) ) ; // false
 * console.log( isObject('hello') ) ; // false
 * console.log( isString(new String('hello')) ) ; // true
 */
const isObject = ( value:any ):boolean =>
{
    const type = typeof(value);
    return value !== null && (type === 'object' || type === 'function') ;
};

export default isObject ;
