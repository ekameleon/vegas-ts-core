/**
 * A basic <code>trace()</code> function based on the console.log method.
 * @name trace
 * @memberOf core
 * @instance
 * @function
 * @version 1.0.0
 * @since 1.0.0
 * @example
 * trace( 'hello world' ) ;
 */
const trace = ( context:any ) =>
{
    if( console )
    {
        console.log( context ) ;
    }
};

export default trace ;
