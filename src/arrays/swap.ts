/**
 * Swaps two indexed values in a specific array representation.
 * @name swap
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} ar - The Array of values to change.
 * @param {number} [from=0] - The first index position to swap.
 * @param {number} [to=0] - The second index position to swap.
 * @param {boolean} [clone=false] Returns a swapped clone of the passed-in array.
 * @example
 * let ar = [ 1 , 2 , 3 , 4 ] ;
 * console.log( ar ) ; // 1,2,3,4
 * swap( ar , 1 , 3 ) ;
 * console.log( ar ) ; // 1,4,3,2
 */
export function swap( ar:any[] , from:number = 0 , to:number = 0 , clone:boolean = false )
{
    if( ar )
    {
        if( clone )
        {
            ar = [ ...ar ] ;
        }
        let value = ar[from] ;
        ar[from] = ar[to] ;
        ar[to]   = value ;
        return ar ;
    }
    return null ;
}

export default swap;
