/**
 * Indicates if the specified character is an alpha (A-Z or a-z) or a digit character.
 * @name isAlphaOrDigit
 * @memberof core.chars
 * @instance
 * @function
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return <code>true</code> if the specified character is an alpha or digit character.
 * @example
 * console.log( isAlphaOrDigit( "Z" ) ) ; // true
 * console.log( isAlphaOrDigit( "a" ) ) ; // true
 * console.log( isAlphaOrDigit( "0" ) ) ; // true
 * console.log( isAlphaOrDigit( "9" ) ) ; // true
 * console.log( isAlphaOrDigit( "+" ) ) ; // false
 */
export function isAlphaOrDigit( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    return (("A" <= c) && (c <= "Z")) || (("a" <= c) && (c <= "z")) || (("0" <= c) && (c <= "9")) ;
}

export default isAlphaOrDigit;
