"use strict" ;

/**
 * Shuffles an array.
 * @name shuffle
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} ar - The array to shuffle.
 * @return {Array} the shuffled array.
 * @example
 * let = [0,1,2,3,4,5,6,7,8,9] ;
 * console.log( ar ) ;
 * shuffle( ar ) ;
 * console.log( ar ) ;
 */
export function shuffle( ar:any[] ):any
{
    if( ar && ar.length > 0 )
    {
        let item:any, rdm:number ;
        let tmp:any[] = [] ;
        let len:number = ar.length;
        let index:number = len - 1 ;
        for ( let i:number = 0 ; i < len ; i++ )
        {
            rdm  = Math.round( Math.random() * index ) ;
            item = ar[ rdm ] ;
            ar.splice( rdm , 1 ) ;
            tmp[tmp.length] = item ;
            index-- ;
        }
        while(--len > -1)
        {
            ar[len] = tmp[len] ;
        }
        return ar ;
    }
    else
    {
        return null ;
    }
}

export default shuffle;
