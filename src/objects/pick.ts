/**
 * Creates an object composed of the picked `object` properties.
 * If the resolved value is `undefined`, the `defaultValue` is returned in its place.
 * @version 1.0.0
 * @since 1.0.0
 * @name pick
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The source object.
 * @param {string[]} [paths] - The property paths to pick.
 * @returns {Object} Returns the new object.
 * @returns {any} Returns the resolved value.
 * @example
 * let source = { a:1 , b:2 , c:3 } ;
 * console.log( pick( source , ['a','c'] ) ) ; // { a:1 , c:3 }
 */
const pick = ( object:any , paths:string[] ):any =>
{
    let result = {} ;
    if( object && paths && paths.length > 0 )
    {
        paths.forEach( key =>
        {
            if( object.hasOwnProperty(key) )
            {
                result = { ...result , [key]:object[key] } ;
            }
        });
    }
    return result ;
};

export default pick ;