/**
 * Returns the first element of the array.
 * @name head
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array reference.
 * @return {any} The first element of the array.
 * @example
 * console.log( head( [2, 3, 4] ) ) ; // 2
 */
const head = ( array:any[] ):any => array[0] ;

export default head ;
