import whiteSpaces from './whiteSpaces'

/**
 * Indicates if the character is white space.
 * @name isWhiteSpace
 * @memberof core.chars
 * @function
 * @instance
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return {boolean} True if the passed-in string value is a white space defines in the core.chars.whiteSpaces collection.
 * @example
 * console.log( isWhiteSpace( '!' ) ) ;
 * console.log( isWhiteSpace( ' ' ) ) ;
 * console.log( isWhiteSpace( '\r' ) ) ;
 */
export function isWhiteSpace( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }

    return whiteSpaces.indexOf(c) > -1 ;
}

export default isWhiteSpace ;
