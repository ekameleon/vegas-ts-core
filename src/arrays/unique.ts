import distinct from './distinct'

/**
 * Creates a new array with unique values.
 * @name unique
 * @since 1.0.0
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array to transform.
 * @param {boolean} last - Indicates if last duplicate element is keeped to fill the new array.
 * @return {Array} The new array with unique values.
 * @example
 * console.log( unique( [1,1,2,3,3,2,4] ) ) ; // [1,2,3,4]
 * console.log( unique( [1,1,2,3,3,2,4] , true ) ) ; // [1,3,2,4]
 */
const unique = ( array:any[] , last:boolean = false ):any[] =>
{
    if ( array )
    {
        if( last )
        {
            array = [ ...array ] ;
            array.reverse() ;
        }
        array = array.filter(distinct) ;
        if( last )
        {
            array.reverse() ;
        }
        return array ;
    }
    return [] ;
};

export default unique ;
