# VEGAS TypeScript

**Vegas TypeScript CORE** - **version 1.0.0** is an *Opensource* Library based on **ECMAScript** and TypeScript for develop crossplatform **Rich Internet Applications** and **Games**.

[![npm version](https://img.shields.io/npm/v/vegas-js-core.svg?style=flat-square?style=flat-square)](https://www.npmjs.com/package/vegas-ts-core)
[![npm downloads](https://img.shields.io/npm/dm/vegas-js-core.svg?style=flat-square)](https://www.npmjs.com/package/vegas-ts-core)

This library contains a set of libraries writing in **Javascript** and based on the [ES6](http://es6-features.org/) standard :

| package  | description                                                                                                                                                                                                                                                                                                                                         |
|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **[core](https://vegasjs.ooopener.com/core.html)**     | The [core](https://vegasjs.ooopener.com/core.html) package is specialized in functions utilities that are highly reusable without creating any dependencies :  arrays, strings, chars, objects, numbers, maths, date, colors, etc.                                                                                                                                                                                          |

### About

 * Author : Marc ALCARAZ (aka eKameleon) - Creative Technologist and Digital Architect
 * Mail : ekameleon[at]gmail.com
 * LinkedIn : [https://www.linkedin.com/in/ekameleon/](https://www.linkedin.com/in/ekameleon/)

### License

Under tree opensource licenses :

 * [License MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
 * [License GPL 2.0+](https://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1+](https://www.gnu.org/licenses/lgpl-2.1.html)

## Resources

#### ⌜ Download

Download on **Bitbucket** the latest code, report an issue, ask a question or contribute :

 * [https://bitbucket.org/ekameleon/vegas-js-core](https://bitbucket.org/ekameleon/vegas-ts-core)

#### ⌜ Slack Community

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the **VEGAS** community on Slack !

## History

 * 2020 : Create the typescript implementation of VEGAS
 * 2018 : Cut the JS packages of VEGAS in a set of independent libraries.
 * 2016 : Begin the new JS architecture of the VEGAS JS library based on ES6
 * 2015 : Google Code must die - **VEGAS** move from an old Google Code SVN repository to this Bitbucket GIT repository and REBOOT this source code.
 * 2007 : Fusion with the Maashaack framework (eden, etc.)
 * 2004 : First official SVN repository
 * 2000 : First framework concept and first libraries (components, tools, design patterns)
 * 1998 : Flash - ActionScript #forever
