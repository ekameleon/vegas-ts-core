import memoize from './memoize.js'

/**
 * A specialized version of `memoize` which clears the memoized function's cache
 * when it exceeds `MAX_MEMOIZE_SIZE` (default 500, you can set this value if you change the memoizeBounded.MAX_MEMOIZE_SIZE value).
 * @name memoizeBounded
 * @memberof core.functors
 * @function
 * @instance
 * @param {Function} func - The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */
const memoizeBounded = ( func:Function ):Function =>
{
    const result = memoize( func, (key:any) =>
    {
        const { cache } = result ;
        if ( cache.size === memoizeBounded.MAX_MEMOIZE_SIZE )
        {
            cache.clear() ;
        }
        return key
    });
    return result ;
};

/**
 * Used as the maximum memoize cache size.
 */
memoizeBounded.MAX_MEMOIZE_SIZE = 500 ;

export default memoizeBounded ;