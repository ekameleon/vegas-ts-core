import memoizeBounded from '../functors/memoizeBounded'

const charCodeOfDot:number = '.'.charCodeAt(0) ;
const reEscapeChar:RegExp = /\\(\\)?/g ;
const rePropName:RegExp = RegExp(
    // Match anything that isn't a dot or bracket.
    '[^.[\\]]+' + '|' +
    // Or match property names within brackets.
    '\\[(?:' +
    // Match a non-string expression.
    '([^"\'].*)' + '|' +
    // Or match strings (supports escaping characters).
    '(["\'])((?:(?!\\2)[^\\\\]|\\\\.)*?)\\2' +
    ')\\]'+ '|' +
    // Or match "" as the space between consecutive dots or empty brackets.
    '(?=(?:\\.|\\[\\])(?:\\.|\\[\\]|$))'
    , 'g'
);

const replacer:Function = ( result:any[] ):Function => (match: string, expression: any, quote: any, subString: any): void => 
{
    let key:string = match;
    if ( quote ) 
    {
        key = subString.replace(reEscapeChar, '$1');
    }
    else if (expression) 
    {
        key = expression.trim();
    }
    result.push(key)
}

/**
 * Converts `string` to a property path array.
 * @private
 * @param {string} source - The string to convert.
 * @returns {Array} Returns the property path array.
 */
const stringToPath = memoizeBounded( ( source:string ) :string[]  =>
{
    const result = [] ;
    if (source.charCodeAt(0) === charCodeOfDot)
    {
        result.push('')
    }
    source.replace(rePropName, replacer(result) );
    return result
});

export default stringToPath ;