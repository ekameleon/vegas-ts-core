import lineTerminators from './lineTerminators'

/**
 * @description Indicates if the specified character is a line terminator :
 * * "\n" - u000A - LF : Line Feed
 * * "\r" - u000D - CR : Carriage Return
 * * ???  - u2028 - LS : Line Separator
 * * ???  - u2029 - PS : Paragraphe Separator
 * @name isLineTerminator
 * @memberof core.chars
 * @function
 * @instance
 * @see <a href="http://www.ecma-international.org/ecma-262/5.1/Ecma-262.pdf">ECMA-262 spec 7.3 (PDF)</a>
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return The value <code>true</code> if the passed-in string value is a line terminator defines in the core.chars.lineTerminators collection.
  * @example
 * console.log( isLineTerminator( "h" ) ) ; // false
 * console.log( isLineTerminator( "\n" ) ) ; // true
 */
export function isLineTerminator( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    let l:number = lineTerminators.length ;
    while( --l > -1 )
    {
        if( c === lineTerminators[l] )
        {
            return true;
        }
    }
    return false ;
}

export default isLineTerminator ;
