import assignValue from './assignValue'
import castPath    from './castPath'
import isIndex     from './isIndex'
import isObject    from '../isObject'
import toKey       from './toKey'

/**
 * The base implementation of `set`.
 * @private
 * @param {Object} object - The object to change.
 * @param {Array|string} path - The path of the property to set.
 * @param {*} value - The value to set.
 * @param {Function} [customizer=null] The function to customize path creation.
 * @returns {Object} Returns the `object` reference.
 */
const baseSet = ( object:any , path:any[]|string|String , value:any, customizer:Function|null = null ):any =>
{
    if (!isObject(object))
    {
        return object ;
    }
    
    path = castPath(path, object) ;

    const length:number = path.length;
    const lastIndex:number = length - 1;

    let index: number = -1;
    let nested:any = object;

    while (nested != null && ++index < length)
    {
        const key:string = toKey(path[index]);
        let newValue:any = value;

        if (index !== lastIndex)
        {
            const objValue:any = nested[key];
            newValue = customizer ? customizer(objValue, key, nested) : undefined;
            if (newValue === undefined)
            {
                newValue = isObject(objValue)
                ? objValue
                : (isIndex(path[index + 1]) ? [] : {});
            }
        }
        
        assignValue(nested, key, newValue);
        nested = nested[key];
    }
    return object
};

export default baseSet ;