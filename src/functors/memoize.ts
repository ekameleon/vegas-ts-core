/**
 * Creates a function that memoizes the result of a specific function.
 * If a resolver Function is provided, it determines the cache key for storing the result based on the arguments provided to the memoized function.
 * By default, the first argument provided to the memoized function is used as the map cache key.
 * The `func` is invoked with the `this` binding of the memoized function.
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 * @name memoize
 * @memberof core.functors
 * @function
 * @instance
 * @param {Function} func - The function to have its output memoized.
 * @param {Function} resolver - The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 * let add = (n) => (n + 10);
 *
 * add = memoize(add);
 *
 * console.log(add(3)) ; // calculated
 * console.log(add(3)) ; // cached
 * console.log(add(4)) ; // calculated
 * console.log(add(4)) ; // cached
 */
const memoize = ( func:Function , resolver:Function|null = null ) :Function =>
{
    if (
        typeof(func) !== 'function' ||
        (resolver != null && typeof(resolver) !== 'function')
    )
    {
        throw new TypeError('memoize, expected a function') ;
    }
    
    const memoized:Function = ( ...args:any[] ) =>
    {
        const key = resolver ? resolver.apply(null, args) : args[0] ;
        const cache = memoized.cache ;
        if ( cache.has(key) )
        {
            return cache.get(key) ;
        }
        const result = func.apply(null, args) ;
        memoized.cache = cache.set(key, result) || cache ;
        return result
    };
    
    memoized.cache = new ( memoize.Cache || Map ) ;
    
    return memoized
};

memoize.Cache = Map ;

export default memoize ;