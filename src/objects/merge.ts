/**
 * Merging enumerable properties from a specific Object to a target Object.
 * @name merge
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {Object} target - The target object to merge.
 * @param {Object} source - The source object reference.
 * @param {boolean} [overwrite=true] - The optional flag to indicates if the merge function can override the already existing properties in the target reference (default true).
 * @return The merged target reference.
 * @example
 * let target = { a : 5 , b : 6 } ;
 * let from   = { a : 1 , b : 2 , c: 3 } ;
 * console.log( dump( merge( target , from ) ) ) ; // {a:1,b:2,c:3}
 */
export default function merge( target:any , source :any, overwrite:boolean = true ):any
{
    if ( overwrite === null || overwrite === undefined )
    {
        overwrite = true ;
    }

    if ( source === null || source === undefined )
    {
        source = {} ;
    }

    for( let prop in source )
    {
        if ( !( prop in target ) || overwrite )
        {
            target[prop] = source[prop] ;
        }
    }

    return target ;
}