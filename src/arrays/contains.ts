/**
 * Determines whether the specified object exists as an element in an Array object.
 * @name contains
 * @memberof core.arrays
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {Array} array - The search Array.
 * @param {any} value - The object to find in the array.
 * @return {boolean} The value <code>true</code> if the specified object exists as an element in the array ; otherwise, <code>false</code>.
 * @example
 * let ar = [2, 3, 4] ;
 * console.log( contains( ar , 3 ) ) ; // true
 * console.log( contains( ar , 5 ) ) ; // false
 */
const contains = ( array:any[]  , value:any ):boolean => array.indexOf(value) > -1 ;

export default contains ;
