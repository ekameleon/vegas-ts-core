import baseSet from './baseSet'

/**
 * Sets the value at `path` of `object`. If a portion of `path` doesn't exist, it's created.
 * Arrays are created for missing index properties while objects are created for all other missing properties.
 * Use `setWith` to customize `path` creation.
 * **Note:** This method mutates `object`.
 * @name get
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {Object} object - The object to change.
 * @param {Array|string} path - The path of the property to set.
 * @param {*} value - The value to set.
 * @returns {Object} Returns the `object` reference.
 * @example
 *
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 *
 * set(object, 'a[0].b.c', 4)
 * console.log(object.a[0].b.c) ; // 4
 *
 * set(object, ['x', '0', 'y', 'z'], 5)
 * console.log(object.x[0].y.z); // 5
 */
const set = ( object:any , path:string|any[] , value:any ):any => object == null ? object : baseSet(object, path, value) ;

export default set ;