/**
 * Indicates if an value is an upper integer.
 * @name isUint
 * @memberof core
 * @function
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 * @param {number} value - The value to evaluates.
 * @return {boolean} <code>true</code> if the passed-in value is an upper integer.
 * @example
 * console.log( isUint(-1) ) ; // false
 * console.log( isUint(0) ) ; // true
 * console.log( isUint(0.5) ) ; // false
 * console.log( isUint(1) ) ; // true
 */
const isUint:Function = ( value:any ):boolean => Number(value) === value && (value%1 === 0) && (value >= 0);

export default isUint ;
