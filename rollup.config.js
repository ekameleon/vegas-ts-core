import babel      from 'rollup-plugin-babel'
import commonjs   from 'rollup-plugin-commonjs'
import license    from 'rollup-plugin-license'
import replace    from 'rollup-plugin-replace'
import resolve    from 'rollup-plugin-node-resolve'
import { terser } from "rollup-plugin-terser"

import pkg from './package.json'
import setting from './build/config.json'

const extensions = [
  '.js', '.jsx', '.ts', '.tsx',
];

const name = 'RollupTypeScriptBabel';

let file = setting.output + setting.file ;
let mode = 'dev' ;

try
{
    switch( process.env.MODE )
    {
        case 'prod' :
        {
            mode = 'prod' ;
            file += '.min.js' ;
            break ;
        }
        default :
        {
            mode = 'dev' ;
            file += '.js' ;
        }
    }
}
catch (e) {}

const plugins =
[
    // Allows node_modules resolution
    resolve({ extensions }),

    // Allow bundling cjs modules. Rollup doesn't understand cjs
    commonjs(),

    // Compile TypeScript/JavaScript files
    babel({ extensions, include: ['src/**/*'] }),

    replace
    ({
        delimiters : [ '<@' , '@>' ] ,
        values     :
        {
            NAME        : pkg.name ,
            DESCRIPTION : pkg.description ,
            HOMEPAGE    : pkg.homepage ,
            LICENSE     : pkg.license ,
            VERSION     : pkg.version
        }
    }),
];

if( mode === 'prod' )
{
    plugins.push( terser() )
}

plugins.push( license({ banner : setting.header }) ) ;

export default
{
    input: './src/index.ts',

    external: [], // https://rollupjs.org/guide/en#external-e-external

    plugins: plugins,

    output:
    [
        {
            name      : setting.bundle ,
            file      : file  ,
            format    : 'umd' ,
            sourcemap : setting.sourcemap && (mode === 'dev') ,
            strict    : true
        }
          /*,
          {
              file: pkg.main , format: 'cjs'
          },
          {
              file: pkg.module , format: 'es',
          },
          {
              file   : pkg.browser,
              format : 'iife',
              name,
              globals: {} // https://rollupjs.org/guide/en#output-globals-g-globals
          }*/
    ],
    watch :
    {
        exclude : 'node_modules/**'
    }
};
