/**
 * Indicates if the specific value is a Function.
 * @version 1.0.0
 * @since 1.0.0
 * @name isObject
 * @memberof core
 * @function
 * @instance
 * @param {Object} value - The value to evaluates.
 * @return {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 * console.log( isFunction(() => {} ) ) ; // true
 * console.log( isFunction(Array.prototype.push) ) ; // true
 */
const isFunction = ( value:any ):boolean =>
{
    const type = typeof(value);
    return value !== null && (type === 'object' || type === 'function') ;
};

export default isFunction ;
