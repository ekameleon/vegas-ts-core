/**
 * Returns the number representation of the passed-in value.
 * @name toNumber
 * @memberof core.numbers
 * @function
 * @instance
 * @param {*} value - The value to transform.
 * @param {*} [defaultValue=''] The default value to return if the value is null or undefined.
 * @return {number} The number representation of passed-in value. Returns `defaultValue` if `value` is `null` or `undefined`.
 * @example
 * let value ;
 * console.log( toNumber(value,12) ) ; // 12
 */
const toNumber = ( value:any , defaultValue:number = 0 ):number =>
{
    if( value === null || value === undefined )
    {
        return defaultValue ;
    }

    if( typeof(value) === 'number' )
    {
        return value ;
    }

    return Number( value ) ;
};

export default toNumber ;
