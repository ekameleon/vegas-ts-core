/**
 * Returns `true` if the given value is not a null Object reference.
 * @name isNotNullObject
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is not null.
 * @example
 * console.log(isNotNullObject(null)); // false
 *
 * console.log(isNotNullObject([])); // true
 * console.log(isNotNullObject({})); // true
 *
 * // primitive variables
 * console.log(isNotNullObject('')); // false
 * console.log(isNotNullObject(0)); // false
 * console.log(isNotNullObject(1)); // false
 * console.log(isNotNullObject(true)); // false
 * console.log(isNotNullObject(false)); // false
 */
const isNotNullObject:Function = ( value:any ):boolean => (typeof(value) === 'object') && (value !== null) ;

export default isNotNullObject ;
