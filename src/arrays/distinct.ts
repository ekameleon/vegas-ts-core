/**
 * Indicates if the specified value exist in the array with a specific index.
 * @name distinct
 * @memberof core.arrays
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {any} value - The object to find in the array.
 * @param {number} index - The index to find the value in the array.
 * @param {Array} array - The search Array.
 * @return {boolean} `true` if the specified value exist in the array with a specific index.
 * @example
 * console.log( distinct( 3 , 0 , [1,2,3] ) ) ; // false
 * console.log( distinct( 3 , 1 , [1,2,3] ) ) ; // false
 * console.log( distinct( 3 , 2 , [1,2,3] ) ) ; // true
 */
const distinct = ( value:any , index:number , array:any[] ):boolean => array.indexOf(value) === index ;

export default distinct ;
