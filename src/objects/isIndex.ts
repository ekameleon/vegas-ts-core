const MAX_SAFE_INTEGER:number= 9007199254740991 ;

// Detect unsigned integer values.
const reIsUint:RegExp = /^(?:0|[1-9]\d*)$/ ;

/**
 * Checks if `value` is a valid array-like index.
 * @private
 * @param {any} value - The value to evaluates.
 * @param {number} [length=MAX_SAFE_INTEGER] - The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
const isIndex = ( value:any , length:number|null = null ):boolean =>
{
    const type:string = typeof(value) ;
    
    length = length == null ? MAX_SAFE_INTEGER : length ;

    return !!length
        && ( type === 'number' || (type !=='symbol' && reIsUint.test(value))) &&
        (value > -1 && value % 1 === 0 && value < length) ;
};

export default isIndex ;