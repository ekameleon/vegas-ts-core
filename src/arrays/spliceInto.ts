/**
 * Splice one array into another.
 * @name spliceInto
 * @memberof core.arrays
 * @function
 * @instance
 * @param {array} inserted - The Array of values inserted in the Array container.
 * @param {array} container - The container modified in place.
 * @param {number} position - The position in the container to inserted the Array of chars.
 * @param {number} count - The count value to replaced values.
 * @example
 * inserted  = [1, 2, 3, 4] ;
 * container = [5, 6, 7, 8] ;
 *
 * console.log( "inserted  : " + inserted  ) ;
 * console.log( "container : " + container ) ;
 *
 * console.log("---") ;
 *
 * inserted  = [1, 2, 3, 4] ;
 * container = [5, 6, 7, 8] ;
 *
 * spliceInto( inserted, container ) ;
 *
 * console.log( "spliceInto( inserted, container, 0 , 0 ) : " + container ) ; // 1,2,3,4,5,6,7,8
 *
 * console.log("---") ;
 *
 * inserted  = [1, 2, 3, 4] ;
 * container = [5, 6, 7, 8] ;
 *
 * spliceInto( inserted, container, 0 , 4 ) ;
 *
 * console.log( "spliceInto( inserted, container, 0 , 4 ) : " + container ) ; // 1,2,3,4
 *
 * console.log("---") ;
 *
 * inserted  = [1, 2, 3, 4] ;
 * container = [5, 6, 7, 8] ;
 *
 * spliceInto( inserted, container, 0 , 2 ) ;
 *
 * console.log( "spliceInto( inserted, container, 0 , 2 ) : " + container ) ; // 1,2,3,4,7,8
 */
export function spliceInto( inserted:any[] , container:any[] , position:number , count:number ):void
{
    inserted.unshift( position , count ) ;
    try
    {
        // @ts-ignore
        container.splice.apply( container , inserted ) ;
    }
    finally
    {
        inserted.splice(0, 2) ;
    }
}

export default spliceInto;
