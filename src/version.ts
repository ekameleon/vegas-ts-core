/**
 * A simple version helper which is composed by four fields : major, minor, build and revision.
 * <p>implementation note:</p>
 * <p>internaly we stock the value of a version (the total of each fields)
 * as a uint which max value is 4294967295 (0xFFFFFFFF) this is done to be able to use operators.</p>
 * <pre class="prettyprint">
 * var v1 = new version( 1, 0 );
 * var v2 = new version( 2, 0 );
 * trace( v1 &lt; v2 ); //true
 * </pre>
 * by default operators in ECMAScript will use the valueOf of the class and this is neat because we can use operators without the need to really implement them.
 *
 * But this cause some little limitations on how much data each fields of a version object can stock
 * <pre>
 *  0x  F F FF FFFF
 *      | |  |   |
 *      | |  |   \revision (max 65535)
 *      | |  \build (max 255)
 *      | \minor (max 15)
 *      \major (max 15)
 * </pre>
 * Here the choice has been to favorise a lot the revision field, favorise a little the build  field, and well give the rest to the major and minor fields.
 *
 * The logic behind that is:
 * - revision should be able to cover the full cycle of a project during all its life
 * - build should be able to cover all the different builds between two minor update
 * - major and minor should cover all the different versions of a project considering you don't update them that much
 *
 * So the max version you can have is v15.15.255.65535
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 */
class version
{
    private _value:number;

    /**
     * Creates a new version instance.
     * @constructor
     * @memberOf core.version
     * @param {number} [major=0] - The major value of the version.
     * @param {number} [minor=0] - The minor value of the version.
     * @param {number} [build=0] - The build value of the version.
     * @param {number} [revision=0] - The revision value of the version.
     */
    constructor( major = 0, minor = 0, build = 0, revision = 0 )
    {
        this._value = (major << 28) | (minor << 24) | (build << 16) | revision ;
    }

    /**
     * Indicates the build value of this version.
     * @name build
     * @memberOf core.version
     * @default 0
     * @type {number}
     */
    get build():number
    {
        return (this._value & 0x00FF0000) >>> 16;
    }

    // noinspection JSAnnotator
    set build( value:number )
    {
        this._value = (this._value & 0xFF00FFFF) | (value << 16);
    }

    /**
     * Indicates the major value of this version.
     * @name build
     * @memberOf core.version
     * @default 0
     * @type {number}
     */
    get major():number
    {
        return this._value >>> 28;
    }

    // noinspection JSAnnotator
    set major( value:number )
    {
        this._value = (this._value & 0x0FFFFFFF) | (value << 28);
    }

    /**
     * Indicates the minor value of this version.
     * @name build
     * @memberOf core.version
     * @default 0
     * @type {number}
     */
    get minor():number
    {
        return (this._value & 0x0F000000) >>> 24;
    }

    // noinspection JSAnnotator
    set minor( value:number )
    {
        this._value = (this._value & 0xF0FFFFFF) | (value << 24);
    }

    /**
     * Indicates the revision value of this version.
     * @name build
     * @memberOf core.version
     * @default 0
     * @type {number}
     */
    get revision():number
    {
        return this._value & 0x0000FFFF;
    }

    // noinspection JSAnnotator
    set revision( value:number )
    {
        this._value = ( this._value & 0xFFFF0000) | value;
    }

    /**
     * Constructs a version object from an unique integer.
     * If the number is zero or negative, or is NaN or Infinity returns an empty version object.
     * @param {number} [value=0] - The integer value to constructs the version object.
     * @memberof core.version
     * @static
     */
    static fromNumber( value:number = 0 ):version
    {
        let v = new version();

        if( isNaN( value ) || (value == 0) || (value < 0) || (value == Number.MAX_VALUE) || (value == Number.POSITIVE_INFINITY) || (value == Number.NEGATIVE_INFINITY) )
        {
            return v;
        }

        v.major    = (value >>> 28);
        v.minor    = (value & 0x0f000000) >>> 24;
        v.build    = (value & 0x00ff0000) >>> 16;
        v.revision = (value & 0x0000ffff);

        return v;
    }

    /**
     * Constructs a version object from a string.
     * @param {string} [value=""] - The integer string to constructs the version object. If the value is null or "", the version expression is 0.0.0.0.
     * @param {string} [separator="."] - The optional separator character.
     * @memberof core.version
     * @static
     */
    static fromString( value:String|string = "", separator:string = "." ):version
    {
        let v = new version();

        if( (value == null) || !(value instanceof String || typeof(value) === 'string' ) || (value == "") )
        {
            return v ;
        }

        if( value.indexOf( separator ) > - 1 )
        {
            let values = value.split( separator );
            let len    = values.length ;

            if( len > 0 )
            {
                v.major = parseInt( values[0] ) ;
            }

            if( len > 1 )
            {
                v.minor = parseInt( values[1] ) ;
            }

            if( len > 2 )
            {
                v.build = parseInt( values[2] ) ;
            }

            if( len > 3 )
            {
                v.revision = parseInt( values[3] ) ;
            }
        }
        else
        {
            v.major = parseInt( value.toString() );
        }

        return v;
    }

    /**
     * Returns the string representation of the object.
     * @name toString
     * @instance
     * @memberOf core.version
     * @function
     * @return {String} the primitive value of the object.
     */
    toString( fields = 0, separator:string = "." )
    {
        let data:number[] = [this.major,this.minor,this.build,this.revision];
        if( (fields > 0) && (fields < 5) )
        {
            data = data.slice( 0, fields );
        }
        else
        {
            let i = 0;
            let l = data.length;
            for( i=l-1 ; i>0 ; i-- )
            {
                if( data[i] === 0 )
                {
                    data.pop();
                }
                else
                {
                    break;
                }
            }
        }

        return data.join( separator );
    }

    /**
     * Returns the primitive value of the object.
     * @name toString
     * @instance
     * @memberOf core
     * @function
     * @return {number} the primitive value of the object.
     */
    valueOf():any
    {
        return this._value;
    }
}

export default version ;
