/**
 * The base implementation to assign a value in a specific object key without value checks.
 * @private
 * @param {Object} object - The object to populate.
 * @param {string} key - The key of the property to assign.
 * @param {*} value - The value to assign.
 */
const baseAssignValue = ( object:any , key:string , value:any ):void =>
{
    if ( key === '__proto__' )
    {
        Object.defineProperty( object, key,
        {
            configurable : true,
            enumerable   : true,
            value        : value,
            writable     : true
        });
    }
    else
    {
        object[key] = value ;
    }
};

export default baseAssignValue ;