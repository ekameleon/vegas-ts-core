import compare            from './compare'
import isAlpha            from './isAlpha'
import isAlphaOrDigit     from './isAlphaOrDigit'
import isASCII            from './isASCII'
import isContained        from './isContained'
import isDigit            from './isDigit'
import isHexDigit         from './isHexDigit'
import isIdentifierStart  from './isIdentifierStart'
import isLineTerminator   from './isLineTerminator'
import isLower            from './isLower'
import isOctalDigit       from './isOctalDigit'
import isOperator         from './isOperator'
import isSymbol           from './isSymbol'
import isUnicode          from './isUnicode'
import isUpper            from './isUpper'
import isWhiteSpace       from './isWhiteSpace'
import lineTerminators    from './lineTerminators'
import operators          from './operators'
import symbols            from './symbols'
import whiteSpaces        from './whiteSpaces'

/**
 * The {@link core.chars} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods to validate and transform the basic characters.
 * <p>You can use this library for example to parse a string (JSON, csv, etc.).</p>
 * @summary The {@link core.chars} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods to validate and transform a basic character.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.chars
 * @memberof core
 */
const chars =
{
    compare           ,
    isAlpha           ,
    isAlphaOrDigit    ,
    isASCII           ,
    isContained       ,
    isDigit           ,
    isHexDigit        ,
    isIdentifierStart ,
    isLineTerminator  ,
    isLower           ,
    isOctalDigit      ,
    isOperator        ,
    isSymbol          ,
    isUnicode         ,
    isUpper           ,
    isWhiteSpace      ,
    lineTerminators   ,
    operators         ,
    symbols           ,
    whiteSpaces
} ;

export default chars ;
