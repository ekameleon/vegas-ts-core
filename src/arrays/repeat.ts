/**
 * Returns a new Array who contains the specified Array elements repeated count times.
 * @name repeat
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} ar - The array to repeat.
 * @param {number} count - The number of repeat.
 * @return {Array} A new Array who contains the specified Array elements repeated count times.
 * @example
 * console.log( repeat( [2, 3, 4] , 0 ) ) ; // 2,3,4
 * console.log( repeat( [2, 3, 4] , 3 ) ) ; // 2,3,4,2,3,4,2,3,4
 */
export function repeat( ar:any[]  , count:number ):any[]
{
    let result:any = null ;
    if( ar )
    {
        count = count > 0 ? count : 0 ;
        if ( count > 0 )
        {
            result = [] ;
            for( let i:number = 0 ; i < count ; i++ )
            {
                result = result.concat(ar) ;
            }
        }
        else
        {
            result = ([] as any[]).concat(ar) ;
        }
    }
    return result ;
}

export default repeat ;
