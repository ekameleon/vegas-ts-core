"use strict" ;

import castPath from './castPath'
import toKey    from './toKey'

/**
 * The base implementation of `get` without support for default values.
 * @private
 * @param {Object} object - The object to query.
 * @param {Array|string} path - The path of the property to get.
 * @returns {any} Returns the resolved value.
 */
const baseGet = ( object:any , path:any[]|string|String ):any =>
{
    path = castPath(path, object) ;

    let index:number = 0 ;
    const length:number = path.length ;

    while ( object !== null && index < length )
    {
        object = object[toKey(path[index++])] ;
    }
    
    return (index && index === length) ? object : undefined ;
};

export default baseGet ;