/**
 * Indicates if the specific object is a Boolean.
 * @name isBoolean
 * @memberof core
 * @function
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 * @param {Object} object - The object to check.
 * @return {boolean} <code>true</code> if the object is a Boolean.
 * @example
 * console.log( isBoolean(0) ) ; // false
 * console.log( isBoolean(true) ) ; // true
 * console.log( isBoolean(false) ) ; // true
 * console.log( isBoolean(3>2) ) ; // true
 * console.log( isBoolean(null) ) ; // false
 */
const isBoolean:Function = ( object:any ):boolean => (typeof(object) === 'boolean') || (object instanceof Boolean ) ;

export default isBoolean ;
