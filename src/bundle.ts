"use strict" ;

/**
 * The vegas-core JS library.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */

import global from './global'

import sayHello , { skipHello } from './hello'

import canUseSymbol from './canUseSymbol'
import dump         from './dump'
import trace        from './trace'
import version      from './version'

import performance from './performance'

import cancelAnimationFrame  from './requestAnimationFrame'
import requestAnimationFrame from './requestAnimationFrame'

import deepMerge         from './deepMerge'
import deepMergeAll      from './deepMergeAll'
import eq                from './eq'
import isBoolean         from './isBoolean'
import isEmpty           from './isEmpty'
import isFloat           from './isFloat'
import isFunction        from './isFunction'
import isInt             from './isInt'
import isMergeableObject from './isMergeableObject'
import isNil             from './isNil'
import isNotEmpty        from './isNotEmpty'
import isNotNull         from './isNotNull'
import isNotNullObject   from './isNotNullObject'
import isNumber          from './isNumber'
import isObject          from './isObject'
import isPlainObject     from './isPlainObject'
import isReactElement    from './isReactElement'
import isString          from './isString'
import isSymbol          from './isSymbol'
import isUint            from './isUint'

import arrays   from './arrays'
import chars    from './chars'
import colors   from './colors'
import date     from './date/index.ts'
import dom      from './dom'
import easings  from './easings'
import functors from './functors'
import graphics from './graphics'
import maths    from './maths'
import numbers  from './numbers'
import objects  from './objects'
import random   from './random'
import reflect  from './reflect'
import strings  from './strings'

const metas = Object.defineProperties( {} ,
{
    name        : { enumerable : true , value : '<@NAME@>'        },
    description : { enumerable : true , value : "<@DESCRIPTION@>" },
    version     : { enumerable : true , value : '<@VERSION@>'     } ,
    license     : { enumerable : true , value : "<@LICENSE@>"     } ,
    url         : { enumerable : true , value : '<@HOMEPAGE@>'    }
});

export default {

    global,

    canUseSymbol,
    dump,
    trace,
    version,

    performance,
    cancelAnimationFrame,
    requestAnimationFrame,

    deepMerge,
    deepMergeAll,
    eq,
    isBoolean,
    isEmpty,
    isFloat,
    isFunction,
    isInt,
    isMergeableObject,
    isNil,
    isNotNull,
    isNotNullObject,
    isNumber,
    isNotEmpty,
    isObject,
    isPlainObject,
    isReactElement,
    isString,
    isSymbol,
    isUint,

    arrays,
    chars,
    colors,
    date,
    dom,
    easings,
    functors,
    graphics,
    maths,
    numbers,
    objects,
    random,
    reflect,
    strings,

    metas,
    sayHello,
    skipHello
}

try
{
    if ( window )
    {
        const load = () =>
        {
            window.removeEventListener( "load", load, false ) ;
            sayHello(metas.name,metas.version,metas.url) ;
        };
        window.addEventListener( 'load' , load , false );
    }
}
catch( ignored ) {}
