import global      from './global'
import performance from './performance'

/**
 * The <code>requestAnimationFrame()</code> method tells the browser that you wish to perform an animation and requests that the browser call a specified function to update an animation before the next repaint. The method takes as an argument a callback to be invoked before the repaint.
 * @module requestAnimationFrame
 * @function
 * @instance
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @param {Function} callback - A parameter specifying a function to call when it's time to update your animation for the next repaint. The callback has one single argument, a <code>DOMHighResTimeStamp</code>, which indicates the current time (the time returned from <code>Performance.now()</code> ) for when requestAnimationFrame starts to fire callbacks.
 * @return {number} A long integer value, the request id, that uniquely identifies the entry in the callback list. This is a non-zero value, but you may not make any other assumptions about its value. You can pass this value to window.cancelAnimationFrame() to cancel the refresh callback request.
 * @example
 * var start = performance.now() ;
 *
 * function step( timestamp )
 * {
 *     var progress = timestamp - start;
 *     console.log( 'step: ' + progress ) ;
 *     if (progress < 2000)
 *     {
 *         id = requestAnimationFrame(step);
 *     }
 * }
 *
 * var id = requestAnimationFrame(step);
 */
const ONE_FRAME_TIME = 16;

let lastTime:number = Date.now();

const vendors:string[] = ['ms', 'moz', 'webkit', 'o'] ;

let len:number = vendors.length ;
for ( let x = 0 ; x < len && !global.requestAnimationFrame ; ++x )
{
    const p:string = vendors[x];
    global.requestAnimationFrame = global[`${p}RequestAnimationFrame`];
    global.cancelAnimationFrame  = global[`${p}CancelAnimationFrame`] || global[`${p}CancelRequestAnimationFrame`];
}

if (!global.requestAnimationFrame)
{
    global.requestAnimationFrame = ( callback:Function ) =>
    {
        if ( typeof callback !== 'function' )
        {
            throw new TypeError(`${callback}is not a function`) ;
        }

        const currentTime = Date.now();

        let delay = ONE_FRAME_TIME + lastTime - currentTime;

        if ( delay < 0 )
        {
            delay = 0;
        }

        lastTime = currentTime;

        return setTimeout(() =>
        {
            lastTime = Date.now();
            callback( performance.now() );
        }, delay);
    };
}

if (!global.cancelAnimationFrame)
{
    global.cancelAnimationFrame = ( id:any ) => clearTimeout(id);
}

export const cancelAnimationFrame = global.cancelAnimationFrame ;

const requestAnimationFrame = global.requestAnimationFrame ;

export default requestAnimationFrame;
