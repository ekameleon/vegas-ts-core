const toString = Object.prototype.toString ;

/**
 * Gets the `toStringTag` value of the specific element.
 * @name getTag
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} object - The object to evaluate.
 * @return {string} Returns the `toStringTag` value of the object.
 * @example
 * console.log( getTag([2,3] ) ; // [object Array]
 */
const getTag = ( object:any ) =>
{
    if ( object == null )
    {
        return object === undefined ? '[object Undefined]' : '[object Null]' ;
    }
    return toString.call(object) ;
};

export default getTag ;
