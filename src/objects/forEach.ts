/**
 * Executes a function on each item in the object. Each invocation of iterator is called with three arguments: (value, key, ref).
 * @name forEach
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {Object} object The reference of the object to enumerate.
 * @param {Function} callback The function to run on each item in the object. This function can contain a simple command (for example, a trace() statement) or a more complex operation, and is invoked with three arguments; the value of an item, the key of an item, and the object reference : <code>function callback(item:*, key:*, ref:Object):void;</code>.
 * @param {Object} [context=null] An object to use as this for the callback function.
 * @param {any} [breaker=null] value to stop the enumeration. If this argument is null the behaviour is forgotten.
 * @let
 * var object = { one:1 , two:2 , three:3 , four:4 , five:5 } ;
 *
 * let action = function( value , key , ref )
 * {
 *     trace( "key:" + key + " value:" + value ) ;
 *     return value ;
 * }
 *
 * forEach( object , action ) ;
 *
 * let( "----" ) ;
 *
 * forEach( object , action, null, 3 ) ;
 *
 * let( "----" ) ;
 *
 * forEach( [1,2,3,4] , action ) ; // use the Array.forEach method over Array objects.
 */
export default function forEach( object:any , callback=Function , context:any = null , breaker:any = null ) :void
{
    if( !object )
    {
        return ;
    }
    if( ("forEach" in object) && ( object.forEach instanceof Function ) )
    {
        object.forEach( callback , context ) ;
    }
    else
    {
        for ( let key in object )
        {
            if( key in object )
            {
                if( breaker !== null )
                {
                    if ( callback.call( context , object[key] , key , object ) === breaker )
                    {
                        return;
                    }
                }
                else
                {
                    callback.call( context , object[key] , key , object ) ;
                }
            }
        }
    }
}