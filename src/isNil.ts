/**
 * Indicates if a value is 'null' or 'undefined'.
 * @name isNil
 * @memberof core
 * @function
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` is the passed-in value is `null` or `undefined`, `false` otherwise
 * @example
 * console.log( isNil( null ) ; // true
 * console.log( isNil( undefined ) ; // true
 * console.log( isNil( 1 ) ; // false
 */
export default function isNil( value:any ):boolean
{
    return value === undefined || value === null;
}
