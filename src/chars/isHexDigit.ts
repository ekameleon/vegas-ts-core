/**
 * Indicates if the specified character is a hexadecimal digit.
 * @name isHexDigit
 * @memberof core.chars
 * @function
 * @instance
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return <true> if the specified character is an hexadecimal digit.
 * @example
 * console.log( isHexDigit( "Z" ) ) ; // false
 * console.log( isHexDigit( "+" ) ) ; // false
 * console.log( isHexDigit( "0" ) ) ; // true
 * console.log( isHexDigit( "1" ) ) ; // true
 * console.log( isHexDigit( "2" ) ) ; // true
 * console.log( isHexDigit( "9" ) ) ; // true
 * console.log( isHexDigit( "A" ) ) ; // true
 * console.log( isHexDigit( "F" ) ) ; // true
 */
export function isHexDigit( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    return ( ("0" <= c) && (c <= "9") ) || (("A" <= c) && (c <= "F")) || (("a" <= c) && (c <= "f")) ;
}

export default isHexDigit ;
