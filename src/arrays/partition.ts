/**
 * Creates an array of elements split into two groups,
 * the first of which contains elements `predicate` returns truthy for,
 * the second of which contains elements `predicate` returns falsey for.
 * The predicate is invoked with one argument: (value).
 * @name partition
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @return {Array} Returns the array of grouped elements.
 * @example
 * const users = [
 *    { name : 'Lancelot'  , active : false },
 *    { name : 'Perceval'  , active : true  },
 *    { name : 'Arthur'    , active : false }
 * ];
 *
 * console.log( partition( users , ({ active }) => active ) ); [['Perceval'], ['Lancelot', 'Arthur']]
 *
 * console.log( partition([1, 2, 3, 4], n => n % 2) ) ; // [ [1, 3], [2, 4] ]
 */
const partition = ( collection:any , predicate:Function ):any =>
{
    const result = [ [] , [] ] ;

    collection.forEach( ( value:any ) =>
    {
        (result[ predicate(value) ? 0 : 1 ] as any).push( value ) ;
    });

    return result ;
};

export default partition ;
