import deepMerge from './deepMerge'

/**
 * Merges any number of objects into a single result object.
 * @name deepMergeAll
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {Array} array - Merge deeply all objects with the deepMerge method behavior.
 * @param {Object} [options] - The second object to merge.
 * @param {function} options.arrayMerge - There are multiple ways to merge two arrays, you can create your own custom function.
 * @param {function} options.isMergeableObject - By default, deepMerge clones every property from almost every kind of object.
 * You may not want this, if your objects are of special types, and you want to copy the whole object instead of just copying its properties.
 * You can accomplish this by passing in a function for the isMergeableObject option.
 * @param {function} options.customMerge - Specifies a function which can be used to override the default merge behavior for a property, based on the property name.
 * The customMerge function will be passed the key for each property, and should return the function which should be used to merge the values for that property.
 * It may also return undefined, in which case the default merge behaviour will be used.
 * @example
 * let a = { foo : { bar : 1 } }
 * let b = { foo : { boo : 2 } }
 * let c = { bar : 'hello' }
 * console.log( deepMergeAll([a, b, c]) ) ;
 * // { foo: { bar : 1, boo : 2 }, bar : 'hello' }
 */
const deepMergeAll = ( array:any[] , options:any = null ) =>
{
    return array.reduce( ( prev , next ) => deepMerge( prev, next, options ) , {} ) ;
};

export default deepMergeAll ;
