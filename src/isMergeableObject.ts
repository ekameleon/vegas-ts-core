import isNotNullObject from './isNotNullObject'
import isReactElement  from './isReactElement'

const isSpecial = ( value:any ):boolean =>
{
    value = Object.prototype.toString.call(value) ;
    return value === '[object RegExp]'
        || value === '[object Date]'
        || isReactElement(value)
};

/**
 * Returns `true` if the given value is a mergeable object.
 * @name isMergeableObject
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is a mergeable object.
 * @example
 * console.log(isMergeableObject(undefined)) ; // false
 * console.log(isMergeableObject(null)) ; // false
 * console.log(isMergeableObject(new RegExp('hello'))) ; // false
 * console.log(isMergeableObject(new Date())) ; // false
 * console.log(isMergeableObject(reactElement)) ; // false
 *
 * console.log(isMergeableObject({})) ; // true
 */
const isMergeableObject = ( value:any ):boolean => isNotNullObject(value) && !isSpecial(value) ;

export default isMergeableObject ;
