import dump    from './dump'
import global  from './global'
import trace   from './trace'
import version from './version'

import cancelAnimationFrame  from './cancelAnimationFrame'
import requestAnimationFrame from './requestAnimationFrame'

import eq                from './eq'
import isBoolean         from './isBoolean'
import isEmpty           from './isEmpty'
import isFloat           from './isFloat'
import isFunction        from './isFunction'
import isInt             from './isInt'
import isMergeableObject from './isMergeableObject'
import isNil             from './isNil'
import isNotEmpty        from './isNotEmpty'
import isNotNull         from './isNotNull'
import isNotNullObject   from './isNotNullObject'
import isNumber          from './isNumber'
import isObject          from './isObject'
import isPlainObject     from './isPlainObject'
import isReactElement    from './isReactElement'
import isString          from './isString'
import isSymbol          from './isSymbol'
import isUint            from './isUint'

import arrays  from './arrays'
import chars   from './chars'
import numbers from './numbers'
import objects from './objects'

/**
 * The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc.
 * <p>You can consider a library as a set of functions organized into classes, here with a <strong>"core"</strong> library in some cases we organize the functions in the package definitions without assembling them into a class.</p>
 * <p>Those functions are allowed to reuse the builtin types (Object, Array, Date, etc.), the Javascript API classes and packages, but nothing else.</p>
 * @summary The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/)|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core
 * @version 1.0.0
 * @since 1.0.0
 */
const core =
{
    global ,
    dump ,
    trace ,
    version ,

    cancelAnimationFrame  ,
    requestAnimationFrame ,

    eq ,
    isBoolean ,
    isEmpty,
    isFloat ,
    isFunction ,
    isInt ,
    isMergeableObject,
    isNil ,
    isNotEmpty,
    isNotNull,
    isNotNullObject,
    isNumber ,
    isObject ,
    isPlainObject,
    isReactElement,
    isString ,
    isSymbol ,
    isUint   ,

    arrays   ,
    chars    ,
    // colors   ,
    // date     ,
    // dom      ,
    // easings  ,
    // functors ,
    // maths    ,
    numbers  ,
    objects  ,
    // random   ,
    // reflect  ,
    // strings
} ;

export default core ;
