import baseGet from './baseGet'

/**
 * Gets the value at `path` of `object`.
 * If the resolved value is `undefined`, the `defaultValue` is returned in its place.
 * @name get
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {Object} object The object to query.
 * @param {Array|string} path - The path of the property to get.
 * @param {*} [defaultValue=null] - The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 *
 * get(object, 'a[0].b.c')
 * // => 3
 *
 * get(object, ['a', '0', 'b', 'c'])
 * // => 3
 *
 * get(object, 'a.b.c', 'default')
 * // => 'default'
 */
const get = ( object:any , path:string|any[] , defaultValue:any = null ):any =>
{
    const result = object === null ? undefined : baseGet(object, path) ;
    return result === undefined ? defaultValue : result
};

export default get ;