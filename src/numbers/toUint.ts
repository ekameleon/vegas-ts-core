/**
 * Transform a float number into this upper integer representation.
 * @name toUint
 * @memberof core.numbers
 * @function
 * @instance
 * @param {number} num - The number to transform in an upper integer numeric value.
 * @return The upper integer representation of the specific numeric value.
 * @example
 * console.log( toUint(0) ) ; // 0
 * console.log( toUint(10) ) ; // 10
 * console.log( toUint(10.123) ) ; // 10
 * console.log( toUint(-10.123) ) ; // 10
 */
const toUint = ( num:number ):number =>
{
    num -= (num % 1) ;
    return num < 0 ? -num : num ;
};

export default toUint ;
