/**
 * Creates an helper to remove a specific property in some objects.
 * @version 1.0.0
 * @since 1.0.0
 * @name removeProperty
 * @memberof core.objects
 * @function
 * @instance
 * @param {string} prop - The name of the property to remove.
 * @return A new function used to remove the property in a specific object reference.
 * @example
 * const user = { id:1, name: 'John Doe', password: 'PWD' } ;
 *
 * const removePassword = removeProperty('password');
 * const removeId       = removeProperty('id') ;
 *
 * console.log( removePassword(user) ) ; // { id:1, name: 'John Doe' }
 * console.log( removeId(user) ) ; // { name: 'John Doe', password: 'PWD' }
 */
const removeProperty = ( prop:string ) => ( { [ prop ] : _ , ...rest } ) => rest ;


export default removeProperty ;