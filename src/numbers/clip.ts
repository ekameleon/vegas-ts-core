/**
 * Bounds a number value between 2 numbers.
 * @name clip
 * @memberof core.numbers
 * @function
 * @instance
 * @param {number} value - The value to clamp.
 * @param {number} min - The min value of the range.
 * @param {number} max - The max value of the range.
 * @return {number} A bound number value between 2 numbers.
 * @example
 * console.log( clip(4, 5, 10) ) ; // 5
 * console.log( clip(12, 5, 10) ) ; // 10
 * console.log( clip(6, 5, 10) ) ; // 6
 */
const clip = ( value:number , min:number , max:number ):number =>
{
    if (value <= min)
    {
        return min ;
    }
    else if (value >= max)
    {
        return max;
    }

    return value ;
};

export default clip ;
