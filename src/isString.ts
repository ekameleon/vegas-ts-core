/**
 * Indicates if the specific object is a String.
 * @name isString
 * @memberof core
 * @function
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 * @param {Object} object - The object to check.
 * @return {boolean} <code>true</code> if the object is a String.
 * @example
 * console.log( isString(0) ) ; // false
 * console.log( isString(true) ) ; // false
 * console.log( isString(null) ) ; // false
 * console.log( isString('hello') ) ; // true
 * console.log( isString(new String('hello')) ) ; // true
 */
const isString:Function = ( object:any ):boolean => (typeof(object) === 'string') || (object instanceof String ) ;

export default isString ;
