import isKey from './isKey'
import stringToPath from '../strings/stringToPath'

/**
 * Casts the passed-in `value` to a path array if it's not one.
 * @private
 * @param {any} value - The value to inspect.
 * @param {Object} [object] - The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
const castPath = (value: any, object: any ) :any[] =>
{
    if ( Array.isArray(value) )
    {
        return value ;
    }
    return isKey(value, object) ? [value] : stringToPath(value) ;
};

export default castPath ;