import canUseSymbol from './canUseSymbol'

const REACT_ELEMENT_TYPE = canUseSymbol ? Symbol.for('react.element') : 0xeac7 ;

/**
 * Returns `true` if the given value is a React element.
 * See https://github.com/facebook/react/blob/b5ac963fb791d1298e7f396236383bc955f916c1/src/isomorphic/classic/element/ReactElement.js#L21-L25
 * @name isReactElement
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is a React element.
 * @example
 * console.log(isReactElement(null)); // false
 * console.log(isReactElement([])); // false
 * console.log(isReactElement({})); // false
 *
 * // React JSX component
 * console.log(isReactElement(<Component/>)); // true
 */
const isReactElement:Function = ( value:any ):boolean => !!value && (value.$$typeof === REACT_ELEMENT_TYPE) ;

export default isReactElement ;
