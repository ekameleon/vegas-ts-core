let object : any ;

try
{
    // @ts-ignore
    object = global || null ;
}
catch( ignore )
{

}

if( !object )
{
    try
    {
        object = window ;
    }
    catch( e )
    {

    }
}

if( !object )
{
    try
    {
        object = document ;
    }
    catch( ignore )
    {

    }
}

if( !object )
{
    object = {} ;
}


/**
 * The <code>global</code> namespace (reference to the global scope of the application), this object can target the <code>window</code> or the <code>document</code> global objects in the browser or an other global reference.
 * @name global
 * @memberof core
 * @const
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 */
const global:any = object ;

export default global ;
