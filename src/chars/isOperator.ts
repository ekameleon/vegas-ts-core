import operators from './operators'

/**
 * Indicates if the passed-in string value is a operator digit.
 * @name isOperator
 * @memberof core.chars
 * @function
 * @instance
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return The value <code>true</code> if the passed-in string value is a operator digit.
 * @example
 * console.log( isOperator( "a" ) ) ; // false
 * console.log( isOperator( "+" ) ) ; // true
 */
export function isOperator( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    return operators.indexOf(c) > -1 ;
}

export default isOperator ;
