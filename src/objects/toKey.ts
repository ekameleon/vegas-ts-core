import isSymbol from '../isSymbol'

const INFINITY = 1 / 0 ;

/**
 * Converts the passed-in object to a string key if it's not a string or symbol.
 * @name toKey
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {any} object - The object to evaluate.
 * @return {string} Returns the string key representation.
 */
const toKey = ( object:any ):string =>
{
    if ( typeof object === 'string' || isSymbol(object))
    {
        return object;
    }
    const result = `${object}` ;
    return (result === '0' && (1 / object) === -INFINITY) ? '-0' : result ;
};

export default toKey ;