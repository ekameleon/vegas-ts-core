/**
 * Performs a [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero) comparison between two values to determine if they are equivalent.
 * @name eq
 * @memberof core
 * @function
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 * @param {*} object1 - The value to compare.
 * @param {*} object2 - The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 * const object = { 'a': 1 }
 * const other = { 'a': 1 }
 *
 * console.log(eq(object, object)); // => true
 * console.log(eq(object, other)); // => false
 * console.log(eq('a', 'a')); // => true
 * console.log(eq('a', Object('a'))); // => false
 * console.log(eq(NaN, NaN)) ;// => true
 */
const eq:Function = ( object1:any , object2:any ):boolean => object1 === object2 || (object1 !== object1 && object2 !== object2) ;

export default eq ;
