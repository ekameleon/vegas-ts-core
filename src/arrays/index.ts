import contains    from './contains'
import distinct    from './distinct'
import head        from './head'
import initialize  from './initialize'
import partition   from './partition'
import pierce      from './pierce'
import repeat      from './repeat'
import rotate      from './rotate'
import shuffle     from './shuffle'
import sortOn      from './sortOn'
import spliceInto  from './spliceInto'
import swap        from './swap'
import tail        from './tail'
import unique      from './unique'

/**
 * The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
 * @summary The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.arrays
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 */
const arrays =
{
    contains ,
    distinct ,
    head ,
    initialize ,
    partition ,
    pierce,
    repeat,
    rotate,
    shuffle,
    sortOn,
    spliceInto,
    swap,
    tail,
    unique
} ;

export default arrays ;
