/**
 * This constant is `true` if the version of javascript is compatible with the Symbol element.
 * @name canUseSymbol
 * @const
 * @type boolean
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @instance
 */
const canUseSymbol:boolean = Boolean((typeof(Symbol) === 'function') && Symbol.for) ;

export default canUseSymbol ;
