# VEGAS TypeScript CORE OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.0] - 2020-02-13
### Added
- First version based on VEGAS JS 1.0.18

