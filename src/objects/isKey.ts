import isSymbol from '../isSymbol'

const reIsDeepProp:RegExp  = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/ ;
const reIsPlainProp:RegExp = /^\w*$/ ;

/**
 * Checks if `value` is a property name and not a property path.
 * @name isKey
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {any} value - The value to check.
 * @param {any} object - The object to queries key on.
 * @return {boolean} Returns `true` if `value` is a property name, else `false`.
 */
const isKey = ( value:any, object:any ):boolean =>
{
    if (Array.isArray(value))
    {
        return false ;
    }
    
    const type = typeof value ;
    
    if (type === 'number' || type === 'boolean' || value == null || isSymbol(value))
    {
        return true
    }
    
    return reIsPlainProp.test(value) || !reIsDeepProp.test(value) || (object !== null && value in Object(object)) ;
};

export default isKey ;