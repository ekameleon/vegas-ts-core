"use strict" ;

import baseGet        from './baseGet'
import castPath       from './castPath'
import clean          from './clean'
import forEach        from './forEach'
import fuse           from './fuse'
import get            from './get'
import getTag         from './getTag'
import isKey          from './isKey'
import members        from './members'
import merge          from './merge'
import pick           from './pick'
import removeProperty from './removeProperty'
import renameProperty from './renameProperty'
import set            from './set'
import toKey          from './toKey'

/**
 * The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra <code>Object</code> methods and implementations.
 * @summary The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra <code>Object</code> methods and implementations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.objects
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 */
const objects =
{
    baseGet,
    castPath,
    clean,
    forEach,
    fuse,
    get,
    getTag,
    isKey,
    members,
    merge,
    pick,
    removeProperty,
    renameProperty,
    set,
    toKey
} ;

export default objects ;