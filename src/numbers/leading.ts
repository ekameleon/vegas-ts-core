/**
 * Adds characters at the beginning of a string representation of a number value.
 * @name leading
 * @memberof core.numbers
 * @function
 * @instance
 * @param {number} value - The value to clamp.
 * @param {number} [count=2] - The minimum number of characters in the string expression.
 * @param {string} [char='0] - The character to insert.
 * @return {string} A string representation of the number value with leading zeros.
 * @example
 * console.log( leading( 1 ) ) ; // 01
 * console.log( leading( 1 , 3 ) ) ; // 003
 * console.log( leading( 1 , 3 , 'x' ) ) ; // xx3
 * console.log( leading( 123 , 5  ) ) ; // 00123
 */
const leading = ( value:number , count:number = 2 , char:string = '0' ):string =>
{
    const isNegative:boolean = Number(value) < 0;

    count = count > 0 ? count : 2 ;

    let buffer:string = value.toString();

    if( isNegative )
    {
        buffer = buffer.slice(1);
    }

    const size:number = count - buffer.length + 1;

    buffer = new Array(size).join(char).concat(buffer);

    return ( isNegative ? '-' : '' ) + buffer ;
};

export default leading ;
