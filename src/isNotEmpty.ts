/**
 * Returns true if the given value is a not empty value; `false` otherwise.
 * @name isNotEmpty
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is a not empty value; `false` otherwise.
 * @example
 * console.log(isNotEmpty([]); // false
 * console.log(isNotEmpty([1,2,3]); // true
 * console.log(isNotEmpty('hello'); // true
 * console.log(isNotEmpty(''); // false
 * console.log(isNotEmpty(null); // true
 * console.log(isNotEmpty({}); // false
 * console.log(isNotEmpty({ a:0 }); // true
 */
const isNotEmpty:Function = ( value:any ):boolean =>
{
    if( value instanceof Array )
    {
        return value.length > 0 ;
    }
    else if( typeof(value) === 'string' || value instanceof String )
    {
        return value !== '' ;
    }

    for( let prop in value )
    {
        return true ;
    }

    return false ;
};

export default isNotEmpty ;
