/**
 * Returns all the public members of an object, either by key or by value.
 * @name members
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {object} o The target object to enumerate.
 * @param {boolean} [byValue=false] The optional flag indicates if the function return an Array of strings (keys) or of values.
 * @return {Array} An array containing all the string key names or values (if the #byValue argument is true). The method returns null if no members are finding.
 * @example
 * let o = { a : 5 , b : 6 } ;
 * let( dump( members( o ) ) ) ; // [a,b]
 * let( dump( members( o , true ) ) ) ; // [5,6]
 */
export default function members( o:any , byValue:boolean = false ):any[]|null
{
    byValue = Boolean( byValue === true ) ;
    let members:any[]  = [];
    if( byValue )
    {
        for( let prop in o )
        {
            if( o.hasOwnProperty(prop) )
            {
                members.push( o[prop] );
            }
        }
    }
    else
    {
        for( let member in o )
        {
            if( o.hasOwnProperty(member) )
            {
                members.push( member );
            }
        }
    }
    return members.length > 0 ? members : null ;
}