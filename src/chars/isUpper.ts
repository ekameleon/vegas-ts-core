/**
 * Indicates if the character is an uppercase letter.
 * @name isUpper
 * @memberof core.chars
 * @function
 * @instance
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return The value <code>true</code> if the specified character is lowercase.
 * @example
 * console.log( isUpper( "A" ) ) ; // true
 * console.log( isUpper( "B" ) ) ; // true
 * console.log( isUpper( "Z" ) ) ; // true
 * console.log( isUpper( "a" ) ) ; // false
 * console.log( isUpper( "b" ) ) ; // false
 * console.log( isUpper( "-" ) ) ; // false
 * console.log( isUpper( "#" ) ) ; // false
 * console.log( isUpper( "1" ) ) ; // false
 */
export function isUpper( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    return ( "A" <= c ) && ( c <= "Z" ) ;
}

export default isUpper ;
