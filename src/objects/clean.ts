const replaceUndefinedOrNull = (key:string, value:any) =>
{
    if ( value === null || value === undefined )
    {
        return undefined;
    }
    return value ;
};

/**
 * Copy the passed-in object and remove all undefined and null properties.
 * @name clean
 * @memberof core.objects
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {Object} object - The source to copy and clean.
 * @return The copy reference.
 * @example
 * let object = { a : 1 , b : null } ;
 * console.log( clean( object ) ) ; // { a : 1 }
 */
export default function clean( object:any ):any
{
    if( object )
    {
        return JSON.parse(JSON.stringify(object, replaceUndefinedOrNull));
    }
    return null ;
}