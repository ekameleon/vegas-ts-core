/**
 * The collection representation of all operators characters.
 * @name operators
 * @memberof core.chars
 * @const
 * @type {Array}
 * @instance
 */
const operators:string[] =
[
    "*" ,
    "/" ,
    "%" ,
    "+" ,
    "-" ,
    "«" ,
    "»" ,
    ">" ,
    "<" ,
    "›" ,
    "&" ,
    "^" ,
    "|"
];

export default operators ;
