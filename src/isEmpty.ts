"use strict" ;

/**
 * Returns true if the given value is an empty value; `false` otherwise.
 * @name isEmpty
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is an empty value; `false` otherwise.
 * @example
 * console.log(isEmpty([]); // true
 * console.log(isEmpty([1,2,3]); // false
 * console.log(isEmpty('hello'); // false
 * console.log(isEmpty(''); // true
 * console.log(isEmpty(null); // false
 * console.log(isEmpty({}); // true
 * console.log(isEmpty({ a:0 }); // false
 */
const isEmpty:Function = ( value:any ):boolean =>
{
    if( value instanceof Array )
    {
        return value.length === 0 ;
    }
    else if( typeof(value) === 'string' || value instanceof String )
    {
        return value === '' ;
    }

    for( let prop in value )
    {
        return false ;
    }
    return true ;
};

export default isEmpty ;
