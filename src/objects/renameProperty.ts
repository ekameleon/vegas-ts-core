/**
 * Creates an helper to rename a property in the objects.
 * @name renameProperty
 * @memberof core.objects
 * @since 1.0.0
 * @version 1.0.0
 * @function
 * @instance
 * @param {string} prop - The name of the property to rename.
 * @param {string} name - The new name of the property
 * @return A new function used to rename a property in a specific object reference.
 * @example
 * const user = { id:1, name: 'John Doe', password: 'PWD' } ;
 * const renameID = renameProperty('id','ID');
 * console.log( renameID(user) ) ; // { ID:1, name: 'John Doe' }
 */
const renameProperty = ( prop:string , name:string ) => ( { [prop] : value , ...rest } ):any => ({ [name]: value, ...rest }); 

export default renameProperty ;