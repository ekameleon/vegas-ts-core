/**
 * Indicates if the specified character is a digit.
 * @name isContained
 * @memberof core.chars
 * @function
 * @instance
 * @param {string} c - The expression to evaluate.
 * @param {number} index - The optional index to evaluate a specific character in the passed-in expression.
 * @param {string} charset - The list of characters to evaluate.
 * @return True if the specified character is a digit.
 * @example
 * console.log( isContained( "a" , "bubble" ) ) ; // false
 * console.log( isContained( "u" , "bubble" ) ) ; // true
 */
export function isContained( c:string , charset:string , index:number = 0):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }

    let l:number = charset.length ;
    for( let i:number = 0 ; i<l ; i++ )
    {
        if( c === charset.charAt( i ) )
        {
            return true ;
        }
    }

    return false;
}


export default isContained;
