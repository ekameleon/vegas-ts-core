import getTag from './objects/getTag'

/**
 * Evaluates if the passed-in value is classified as a `Symbol` primitive or object.
 * @version 1.0.0
 * @since 1.0.0
 * @name isSymbol
 * @memberof core
 * @function
 * @instance
 * @param {*} object - The object to evaluate.
 * @return {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 * console.log( isSymbol(Symbol.iterator) ) ; // true
 * console.log( isSymbol('abc') ) ; // false
 */
const isSymbol:Function = ( object:any ):boolean =>
{
    const type = typeof object ;
    return type === 'symbol' || (type === 'object' && object !== null && getTag(object) === '[object Symbol]') ;
};

export default isSymbol ;
