/**
 * Indicates if the specified character is a digit.
 * @name isDigit
 * @memberof core.chars
 * @function
 * @instance
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return {boolean} True if the specified character is a digit.
 * @example
 * console.log( isDigit( "Z" ) ) ; // false
 * console.log( isDigit( "+" ) ) ; // false
 * console.log( isDigit( "0" ) ) ; // true
 * console.log( isDigit( "9" ) ) ; // true
 */
export function isDigit( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    return ("0" <= c) && (c <= "9") ;
}

export default isDigit ;
