/**
 * Indicates if the character is lowercase.
 * @name isLower
 * @memberof core.chars
 * @function
 * @instance
 * @param {string} c - The expression to evaluate.
 * @param {number} [index=0] - The optional index to evaluate a specific character in the passed-in expression.
 * @return The value <code>true</code> if the specified character is lowercase.
 * @example
 * console.log( isLower( "a" ) ) ; // true
 * console.log( isLower( "A" ) ) ; // false
 * console.log( isLower( "-" ) ) ; // false
 * console.log( isLower( "#" ) ) ; // false
 * console.log( isLower( "1" ) ) ; // false
 */
export function isLower( c:string , index:number = 0 ):boolean
{
    if( index > 0 )
    {
        c = c.charAt( index ) ;
    }
    return ( "a" <= c ) && ( c <= "z" ) ;
}

export default isLower ;
