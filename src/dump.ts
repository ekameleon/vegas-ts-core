import toUnicodeNotation from './numbers/toUnicodeNotation'

/**
 * Dumps a string representation of any object reference.
 * @name dump
 * @memberof core
 * @function
 * @instance
 * @version 1.0.0
 * @since 1.0.0
 * @param {*} value - Any object to dump.
 * @param {boolean} [prettyprint=false] boolean option to output a pretty printed string
 * @param {number} [indent=0] initial indentation
 * @param {string} [indentor=    ] initial string used for the indent.
 * @return The string expression of the dump.
 * @example
 * let object =
 * {
 *     name   : "vegas" ,
 *     count  : 10 ,
 *     time   : new Date() ,
 *     flag   : true ,
 *     values : [1,2,3]
 * } ;
 * trace( dump( object ) ) ;
 */
export default function dump( value:any , prettyprint:boolean = false , indent:number = 0  , indentor:string = "    " ):string
{
    indent = isNaN(indent) ? 0 : indent ;

    prettyprint = Boolean( prettyprint ) ;

    if( !indentor )
    {
        indentor = "    " ;
    }

    if( value === undefined )
    {
        return "undefined";
    }
    else if( value === null )
    {
        return "null";
    }
    else if( typeof(value) === "string" || value instanceof String )
    {
        return dumpString( value.toString() );
    }
    else if ( typeof(value) === "boolean" || value instanceof Boolean  )
    {
        return value ? "true" : "false";
    }
    else if( typeof(value) === "number" || value instanceof Number )
    {
        return value.toString() ;
    }
    else if( value instanceof Date )
    {
        return dumpDate( value );
    }
    else if( value instanceof Array )
    {
        return dumpArray( value , prettyprint, indent, indentor );
    }
    else if( value.constructor && value.constructor === Object )
    {
        return dumpObject( value , prettyprint, indent, indentor );
    }
    else if( "toSource" in value )
    {
        return value.toSource( indent );
    }
    else
    {
        return '<unknown>';
    }
}

/**
 * Dumps a string representation of any Array reference.
 * @name dumpArray
 * @memberof core
 * @function
 * @instance
 * @param {Array} value - The Array to dump.
 * @param {boolean} [prettyprint=false] boolean option to output a pretty printed string
 * @param {number} [indent=0] initial indentation
 * @param {string} [indentor=    ] initial string used for the indent.
 * @return The dump string representation of any Array reference.
 */
export function dumpArray( value:any[] , prettyprint:boolean = false , indent:number = 0 , indentor:string = "    " ):string
{
    indent = isNaN(indent) ? 0 : indent ;
    prettyprint = Boolean( prettyprint ) ;

    if( !indentor )
    {
        indentor = "    " ;
    }

    let source = [];

    let i ;
    let l = value.length ;

    for( i = 0 ; i < l ; i++ )
    {
        if( value[i] === undefined )
        {
            source.push( "undefined" );
            continue;
        }
        if( value[i] === null )
        {
            source.push( "null" );
            continue;
        }
        if( prettyprint )
        {
            indent++ ;
        }
        source.push( dump( value[i], prettyprint, indent, indentor ) ) ;
        if( prettyprint )
        {
            indent-- ;
        }
    }
    if( prettyprint )
    {
        let spaces  = [] ;
        for( i=0 ; i < indent ; i++ )
        {
            spaces.push( indentor );
        }
        let decal  = '\n' + spaces.join( '' ) ;
        return decal + "[" + decal + indentor + source.join( "," + decal + indentor ) + decal + "]" ;
    }
    else
    {
        return "[" + source.join( "," ) + "]" ;
    }
}

/**
 * Dumps a string representation of any Date reference.
 * @name dumpDate
 * @memberof core
 * @function
 * @instance
 * @param {Date} date - A Date object to dump.
 * @param {boolean} [timestamp=false] - The optional timestamp flag.
 * @return The string representation of any Date reference.
 */
export function dumpDate( date:Date , timestamp:boolean = false  )
{
    timestamp = Boolean( timestamp ) ;
    if ( timestamp )
    {
        return "new Date(" + String( date.valueOf() ) + ")";
    }
    else
    {
        let y    = date.getFullYear();
        let m    = date.getMonth();
        let d    = date.getDate();
        let h    = date.getHours();
        let mn   = date.getMinutes();
        let s    = date.getSeconds();
        let ms   = date.getMilliseconds();
        let data = [ y, m, d, h, mn, s, ms ];
        data.reverse();
        while( data[0] === 0 )
        {
            data.splice( 0, 1 );
        }
        data.reverse() ;
        return "new Date(" + data.join( "," ) + ")";
    }
}


/**
 * Dumps a string representation of an object.
 * @name dumpObject
 * @memberof core
 * @function
 * @instance
 * @param {Object} value - An object to dump.
 * @param {boolean} [prettyprint=false] - The option to output a pretty printed string.
 * @param {number} [indent=0] - The initial indentation value.
 * @param {string} [indentor=    ] - The initial string used for the indent.
 * @return The string expression of the dump.
 */
export function dumpObject( value:any , prettyprint:boolean = false , indent:number = 0 , indentor:string = "    " )
{
    indent = isNaN(indent) ? 0 : indent ;

    prettyprint = Boolean( prettyprint ) ;

    if( !indentor )
    {
        indentor = "    " ;
    }

    let source  = [];

    for( let member in value )
    {
        if( value.hasOwnProperty( member ) )
        {
            if( value[member] === undefined )
            {
                source.push( member + ":" + "undefined" );
                continue;
            }

            if( value[member] === null )
            {
                source.push( member + ":" + "null" );
                continue;
            }

            if( prettyprint )
            {
                indent++ ;
            }

            source.push( member + ":" + dump( value[ member ], prettyprint, indent, indentor ) );

            if( prettyprint )
            {
                indent-- ;
            }
        }
    }
    source = source.sort();
    if( prettyprint )
    {
        let spaces = [];
        for( let i = 0 ; i < indent ; i++ )
        {
            spaces.push( indentor );
        }

        let decal = '\n' + spaces.join( '' );
        return decal + '{' + decal + indentor + source.join( ',' + decal + indentor ) + decal + '}' ;
    }
    else
    {
        return( '{' + source.join( ',' ) + '}' ) ;
    }
}

/**
 * Dumps a string representation of any String value.
 * @name dumpString
 * @memberof core
 * @function
 * @instance
 * @param {string} value a String to transform.
 * @return The dump string representation of any String value.
 */
export function dumpString( value:string ):string
{
    let code  ;
    let quote  = "\"" ;
    let str    = ""  ;
    let ch     = ""  ;
    let pos    = 0   ;
    let len    = value.length ;
    while( pos < len )
    {
        ch  = value.charAt( pos );
        code = value.charCodeAt( pos );
        if( code > 0xFF )
        {
            str += "\\u" + toUnicodeNotation( code );
            pos++;
            continue;
        }
        switch( ch )
        {
            case "\u0008" : // backspace
            {
                str += "\\b" ;
                break;
            }
            case "\u0009" : // horizontal tab
            {
                str += "\\t" ;
                break;
            }
            case "\u000A" : // line feed
            {
                str += "\\n" ;
                break;
            }
            case "\u000B" : // vertical tab /* TODO: check the VT bug */
            {
                str += "\\v" ; //str += "\\u000B" ;
                break;
            }
            case "\u000C" : // form feed
            {
                str += "\\f" ;
                break;
            }
            case "\u000D" : // carriage return
            {
                str += "\\r" ;
                break;
            }
            case "\u0022" : // double quote
            {
                str += "\\\"" ;
                break;
            }
            case "\u0027" : // single quote
            {
                str += "\\\'";
                break;
            }
            case "\u005c" : // backslash
            {
                str += "\\\\";
                break;
            }
            default :
            {
                str += ch;
            }
        }
        pos++;
    }
    return quote + str + quote;
}
