import isMergeableObject from './isMergeableObject'

const defaultMergeable = isMergeableObject;

const cloneUnlessOtherwiseSpecified = ( value:any , options:any ) => (options && (options.clone !== false) && options.isMergeableObject(value) )
                                    ? deepMerge( Array.isArray(value) ? [] : {} , value, options)
                                    : value ;

const defaultArrayMerge = ( target:any , source:any, options:any ) => target.concat(source).map( (element:any) => cloneUnlessOtherwiseSpecified(element, options) ) ;

const getMergeFunction = ( key:any , options:any ) =>
{
    if (options )
    {
        let { customMerge } = options ;
        if( typeof customMerge === 'function'  )
        {
            customMerge = customMerge(key) ;
            if( typeof customMerge === 'function' )
            {
                return customMerge ;
            }
        }
    }
    return deepMerge ;
};

const mergeObject = ( target:any , source:any, options:any ) =>
{
    let destination:any = {} ;

    if ( options )
    {
        const { isMergeableObject } = options ;
        if( typeof isMergeableObject === 'function' )
        {
            if( isMergeableObject(target) )
            {
                Object.keys(target).forEach( key =>
                {
                    destination[key] = cloneUnlessOtherwiseSpecified(target[key], options) ;
                })
            }
        }
    }

    Object.keys(source).forEach( key =>
    {
        if (!options.isMergeableObject(source[key]) || !target[key])
        {
            destination[key] = cloneUnlessOtherwiseSpecified( source[key] , options ) ;
        }
        else
            {
            destination[key] = getMergeFunction(key, options)(target[key], source[key], options) ;
        }
    });

    return destination
};

/**
 * Merge deeply two objects, returning a new merged object with the elements from both.
 * If an element at the same key is present for both, the value from the second object will appear in the result.
 * Merging creates a new object and by default, arrays are merged by concatenating them.
 * @name deepMerge
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} target - The first object to merge.
 * @param {*} source - The second object to merge.
 * @param {Object} [options] - The optional object to configure the merge behavior.
 * @param {function} options.arrayMerge - There are multiple ways to merge two arrays, you can create your own custom function.
 * @param {function} options.isMergeableObject - By default, deepMerge clones every property from almost every kind of object.
 * You may not want this, if your objects are of special types, and you want to copy the whole object instead of just copying its properties.
 * You can accomplish this by passing in a function for the isMergeableObject option.
 * @param {function} options.customMerge - Specifies a function which can be used to override the default merge behavior for a property, based on the property name.
 * The customMerge function will be passed the key for each property, and should return the function which should be used to merge the values for that property.
 * It may also return undefined, in which case the default merge behaviour will be used.
 * @return {Object} the new object representation.
 * @example <caption>1. Basic usage</caption>
 * let user1 =
 * {
 *     user  : { name : 'ekameleon' },
 *     roles : [ { name  : 'admin', value : [ 1, 2, 3 ] }]
 * };
 *
 * let user2 =
 * {
 *     user    : { age : 42 },
 *     roles   : [ { name  : 'superadmin', value : [ 4, 5, 6 ] }] ,
 *     options : { isAdmin : 'yes' }
 * };
 *
 * console.log( deepMerge(user1, user2) ) ;
 *
 * @example <caption>2. Overwrite Array</caption>
 * const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray ;
 *
 * console.log( deepMerge([1, 2, 3], [3, 2, 1], { arrayMerge: overwriteMerge } ) ) ; // 3,2,1
 * console.log( deepMerge(user1, user2, { arrayMerge: overwriteMerge }) ) ;
 *
 * @example <caption>3. Combine Array</caption>
 * const emptyTarget = value => Array.isArray(value) ? [] : {} ;
 * const clone = (value, options) => merge( emptyTarget(value), value, options )  ;
 *
 * const combineMerge = (target, source, options) =>
 * {
 *     const destination = [ ...target ] ;
 *     source.forEach( (e, i) =>
 *     {
 *         if (typeof destination[i] === 'undefined')
 *         {
 *             const cloneRequested = options.clone !== false ;
 *             const shouldClone    = cloneRequested && options.isMergeableObject(e) ;
 *             destination[i]       = shouldClone ? clone(e, options) : e ;
 *         }
 *         else if (options.isMergeableObject(e))
 *         {
 *             destination[i] = deepMerge(target[i], e, options) ;
 *         }
 *         else if (target.indexOf(e) === -1)
 *         {
 *             destination.push(e) ;
 *         }
 *     })
 *     return destination ;
 * }
 *
 * console.log( deepMerge( [{ a: true }], [{ b: true }, 'hello'], { arrayMerge: combineMerge })) ;
 * // [{ a: true, b: true }, 'hello']
 *
 * @example <caption>4. Custom merge</caption>
 * let user1 =
 *  {
 *      name :
 *      {
 *          first : 'John',
 *          last  : 'Doe'
 *     },
 *      pets : ['Cat', 'Parrot']
 *  };
 *
 *  let user2 =
 *  {
 *      name :
 *      {
 *          first : 'Jeanne',
 *          last  : 'Doe'
 *      },
 *     pets : ['Dog']
 * };
 *
 * const mergeNames = (nameA, nameB) => `${nameA.first} and ${nameB.first}`
 * const options =
 * {
 *     customMerge : (key) =>
 *     {
 *         if( key === 'name')
 *         {
 *             return mergeNames
 *         }
 *     }
 * };
 *
 * const user = deepMerge(user1, user2, options)
 *
 * console.log(user.name) ; // John and Jeanne
 * console.log(user.pets) ; // ['Cat', 'Parrot', 'Dog']
 */
const deepMerge = ( target:any , source:any, options:any = null ):any =>
{
    options = options || {} ;

    const {
        arrayMerge,
        isMergeableObject
    } = options ;

    options.arrayMerge        = arrayMerge || defaultArrayMerge ;
    options.isMergeableObject = isMergeableObject || defaultMergeable ;

    const sourceIsArray = Array.isArray(source) ;
    const targetIsArray = Array.isArray(target) ;

    const sourceAndTargetTypesMatch = sourceIsArray === targetIsArray ;

    if (!sourceAndTargetTypesMatch)
    {
        return cloneUnlessOtherwiseSpecified(source, options) ;
    }
    else if ( sourceIsArray )
    {
        return options.arrayMerge(target, source, options) ;
    }
    else
    {
        return mergeObject(target, source, options) ;
    }
};

export default deepMerge ;
