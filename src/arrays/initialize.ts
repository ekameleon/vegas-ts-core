/**
 * Initializes a new Array with an arbitrary number of elements (index), with every element containing the passed parameter value or by default the null value.
 * @name initialize
 * @memberof core.arrays
 * @function
 * @instance
 * @param {number} [elements=1] - The number of elements to fill the Array.
 * @param {*} [value=null] - The value to inject in the Array.
 * @return A new Array with an arbitrary number of elements (index), with every element containing the passed parameter value or by default the null value.
 * @example
 * ar = initialize( 3 ) ;
 * console.log( ar ) ; // [ null , null , null ]
 *
 * ar = initialize( 3 , 0 ) ;
 * console.log( ar ) ; // [ 0 , 0 , 0 ]
 *
 * ar = initialize( 3 , true ) ;
 * console.log( ar ) ; // [ true , true , true ]
 *
 * ar = initialize(  4 , "" ) ;
 * console.log( ar ) ; // [ "" ,"" ,"" ,"" ]
 */
export function initialize( elements:number = 0 , value:any = null ):any[]
{
    const ar:any[] = [];
    elements = elements > 0 ? Math.abs(elements) : 0 ;
    if( elements > 0 )
    {
        for( let i:number = 0 ; i < elements ; i++ )
        {
            ar[i] = value ;
        }
    }
    return ar;
}

export default initialize ;
