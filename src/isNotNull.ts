/**
 * Returns `true` if the given value is not null.
 * @name isNotNull
 * @memberof core
 * @version 1.0.0
 * @since 1.0.0
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is not null.
 * @example
 * console.log(isNotNull(null)); // false
 *
 * console.log(isNotNull([])); // true
 * console.log(isNotNull({})); // true
 *
 * console.log(isNotNull('')); // true
 * console.log(isNotNull(0)); // true
 * console.log(isNotNull(1)); // true
 * console.log(isNotNull(true)); // true
 * console.log(isNotNull(false)); // true
 */
const isNotNull:Function = ( value:any ) => value !== null ;

export default isNotNull ;
